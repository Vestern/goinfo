package main

import (
	"fmt"
	"os"
	"os/user"
	"strconv"
	"time"

	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"
)

type info struct {
	username string
	uid      string
	hostname string
	cwd      string
	boott    uint64
	upt      uint64

	kernelv   string
	hplatform string
	hfamily   string
	hversion  string

	host    host.InfoStat
	loadavg load.AvgStat

	cpu []cpu.InfoStat

	mem  mem.VirtualMemoryStat
	swap mem.SwapMemoryStat

	partitions []disk.PartitionStat

	//procmisc load.MiscStat

}

func dashuman(d uint64) string {
	human := [5]string{" KB", " MB", " GB", " TB", " PB"}

	f := float64(d)

	cur := " Byte"
	for i := range human {
		if f/1000 < 1 {
			return strconv.FormatFloat(f, 'f', 2, 64) + cur
		}
		f = f / 1000
		cur = human[i]
	}
	return strconv.FormatFloat(f, 'f', 2, 64) + cur
}

func main() {
	i := info{}
	usr, err := user.Current()
	if err != nil {
		fmt.Println("Error getting user: ", err)
		return
	}

	i.username = usr.Username
	i.uid = usr.Uid

	hostname, err := os.Hostname()
	if err != nil {
		fmt.Println("Error getting hostname: ", err)
		return
	}

	i.hostname = hostname

	cwd, err := os.Getwd()
	if err != nil {
		fmt.Println("Error getting cwd: ", err)
		return
	}

	i.cwd = cwd

	hi, err := host.Info()
	if err != nil {
		fmt.Println(err)
		return
	}

	i.host = *hi

	ci, err := cpu.Info()
	if err != nil {
		fmt.Println("Error getting cpuinfo: ", err)
		return
	}

	i.cpu = ci

	li, err := load.Avg()
	if err != nil {
		fmt.Println(err)
		return
	}
	i.loadavg = *li

	mi, err := mem.VirtualMemory()
	if err != nil {
		fmt.Println(err)
		return
	}
	i.mem = *mi

	si, err := mem.SwapMemory()
	if err != nil {
		fmt.Println(err)
		return
	}
	i.swap = *si

	pai, err := disk.Partitions(true)
	if err != nil {
		fmt.Println(err)
		return
	}
	i.partitions = pai

	/* 	lmi, err := load.Misc()
	   	if err != nil {
	   		fmt.Println(err)
	   		return
	   	}
	   	i.procmisc = *lmi */

	fmt.Printf("Username:            %s (%s)\n", i.username, i.uid)
	fmt.Printf("Hostname:            %s\n", i.hostname)
	fmt.Printf("Directory:           %s\n", i.cwd)
	t := time.Unix((int64)(i.host.BootTime), 0)
	fmt.Printf("Uptime (Boot time):  %d (%s)\n", i.host.Uptime, t)
	fmt.Printf("OS:                  %s\n", i.host.OS)
	fmt.Printf("Platform:            %s %s\n", i.host.Platform, i.host.PlatformVersion)
	fmt.Printf("Kernel Version:      %s\n", i.host.KernelVersion)
	fmt.Printf("Load:                %.1f %.1f %.1f\n", i.loadavg.Load1, i.loadavg.Load5, i.loadavg.Load15)
	for _, e := range i.cpu {
		fmt.Printf("CPU ID:              %d\n", e.CPU)
		fmt.Printf("\tVendor:      %s\n", e.VendorID)
		fmt.Printf("\tModel:       %s\n", e.ModelName)
		fmt.Printf("\tCores:       %d\n", e.Cores)
		fmt.Printf("\tClock (Mhz): %.2f\n", e.Mhz)
		fmt.Printf("\tCache:       %d\n", e.CacheSize)
	}
	fmt.Printf("Memory:              %s/%s (%.1f%%)\n", dashuman(i.mem.Used), dashuman(i.mem.Total), i.mem.UsedPercent)
	fmt.Printf("\tTotal:       %s\n", dashuman(i.mem.Total))
	fmt.Printf("\tUsed:        %s\n", dashuman(i.mem.Used))
	fmt.Printf("\tFree:        %s\n", dashuman(i.mem.Free))
	fmt.Printf("\tAvailable:   %s\n", dashuman(i.mem.Available))
	fmt.Printf("Swap:                %d/%d (%.1f%%)\n", i.swap.Used, i.swap.Total, i.swap.UsedPercent)
	for _, e := range i.partitions {
		fmt.Printf("Mount:               %s\n", e.Mountpoint)
		u, err := disk.Usage(e.Mountpoint)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("\tUsage:       %d/%d (%f%%)\n", u.Used, u.Total, u.UsedPercent)
		fmt.Printf("\tFree:        %s\n", dashuman(u.Free))
		fmt.Printf("\tDevice:      %s %s\n", e.Fstype, e.Device)
	}

}
